# Instructions
1. add the void40 folder into the handwired directory in QMK.
2. run qmk compile -kb handwired/void40 -km colemak_dh_grid
3. run qmk flash  ~/qmk_firmware/.build/*.hex
4. This will then prompt you to hit the reset on the keyboard.
5. Done!!!! 

# Credit
Credit goes to: https://github.com/victorlucachi/void40
This is a fork of what he has written.
